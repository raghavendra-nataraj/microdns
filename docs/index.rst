.. MicroDNS documentation master file, created by
   sphinx-quickstart on Mon Dec 10 14:39:04 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to MicroDNS' documentation!
====================================

MicroDNS is a mock DNS used in conjunction with other tools (MicroServer, TrafficReplay) to test Apache Traffic Server. It is designed to provide a simple DNS interface to redirect domains to custom IPs for testing.

Installation
-----------------
MicroDNS depends on:

* `TRlib <https://pypi.org/project/trlib/>`_
* dnslib

Using MicroDNS
-----------------

The MicroDNS module provides the following functions:

**MicroDNS**\ (*zone_file, port, ip='127.0.0.1', rr=False*)
    * *zone_file* - JSON file containing the domains and IPs (required)
    * *port* - port for MicroDNS to run on (required)
    * *ip* - IP address for MicroDNS to run on (default: 127.0.0.1)
    * *rr* - if MicroDNS should round robin select IPs for a domain if there are multiple IPs present (default: False)

    These are also the same arguments needed to launch MicroDNS from the command line.

MicroDNS.\ **serve_forever**\ ()
    This will spawn 2 threads that launches a threading TCP server and a threading UDP server. 
    This function is called automatically if MicroDNS is launched from the command line; if MicroDNS is instantiated, the user will have to call this function manually to start the servers.
    MicroDNS will continue to run until it receives a SigINT. 


MicroDNS' *zone_file* is formmated as following:

.. code-block:: python

    { 
        "mappings": [{"abc.com": "127.0.0.2"}, {"foo.com": ["127.0.0.3", "127.0.0.4"]}],
        "otherwise": ["127.0.0.1"]
    }

The `otherwise` option is used when MicroDNS can't find a matching domain.

TODOs
-----------------

* Make TCP DNS server optional 



.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
