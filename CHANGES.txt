v1.0.0  Dec. 3, 2018 -- Initial release

v1.0.1  Aug. 28, 2019 -- Fixed a bug where the UDP and TCP handlers would falsely strip bytes. Bug and fix is detailed in https://github.com/apache/trafficserver/issues/5793
